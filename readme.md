# TDD

- Cloner ce repo
- Rechercher et expliquer avec vos mots les principes du tdd, et l'ajouter a ce Readme.
- Observer fizzbuzz.test.js puis completer fizzbuzz.js pour qu'il passe les tests ("npm run test" pour lancer les tests)
- Creer un fichier calc.js dans /src contenant une fonction calc()
- Creer un fichier calc.test.js dans /test
- Ecrire les test d'une fonction calc qui permet d'additionner/soustraire/multiplier/diviser 2 nombres envoyé a la fonction calc, 
avec gestion de toutes les erreurs possibles, en utilisant la méthode tdd.
- commit entre chaque ecriture de test et chaque ecriture de fonction

### principes du tdd

1. Il ne manipule aucun fichier
2. Il peut s’exécuter en même temps que les autres tests unitaires
3. Il ne doit pas être lié à une autre fonctionnalité ou à un fichier de configuration pour être exécuté
4. Chaque fonction unitaire de l’application possède son propre test unitaire, écrit avant le code
5. Il doit alors être le plus simple et le plus réduit possible