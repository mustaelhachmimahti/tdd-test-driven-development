const expect = require('chai').expect;
const {calc} = require('../src/calc');

describe("Calc", () => {
    const numOne= 15
    const numTwo= 7
    it(`Should multiply correctly, result should be ${calc(numOne, numTwo, "*")}`, () => {
        expect(calc(numOne, numTwo, "*")).to.equal(calc(numOne, numTwo, "*"));
    });
    it(`Should add correctly, result should be ${calc(numOne, numTwo, "+")}`, () => {
        expect(calc(numOne, numTwo, "+")).to.equal(calc(numOne, numTwo, "+"));
    });
    it(`Should subtract correctly, result should be ${calc(numOne, numTwo, "-")}`, () => {
        expect(calc(numOne, numTwo, "-")).to.equal(calc(numOne, numTwo, "-"));
    });
    it(`Should divide correctly, result should be ${calc(numOne, numTwo, "/")}`, () => {
        expect(calc(numOne, numTwo, "/")).to.equal(calc(numOne, numTwo, "/"));
    });
})