const fizzbuzz = (num) => {
    if(num === undefined) return 'Error!'
    if(num % 5 == 0 && num % 7 == 0) return 'fizzbuzz'
    return (num % 5 == 0 ? "buzz" : (num % 7 == 0?"fizz":""));
}

exports.fizzbuzz = fizzbuzz;