const calc = (numOne, numTwo, sign) => {
    switch (sign){
        case "+": return numOne + numTwo;
        case "-": return numOne - numTwo;
        case "*": return numOne * numTwo;
        case "/": return numOne / numTwo;
    }
}

exports.calc = calc;